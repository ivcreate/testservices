В каждом микросервисе нужно:
1) в файле .env прописать нужую базу
2) указать APP_URL в виде http://{ваш домен}
3) APP_DEBUG = false (необязательно)

Для запуска acceptpay:
1) слушатель php artisan queue:listen --queue=add_pay_user
2) выполнить в корне сервиса для создания таблиц php artisan migrate

Для запуска paysystem:
1) в cron добавить * * * * * cd domains\test.test\services\paysystem && php artisan schedule:run
2) слушатель php artisan queue:listen --queue=send_pay
3) выполнить в корне сервиса для создания таблиц php artisan migrate

Url генерации пополнений http://{ваш домен}/services/paysystem/public/
