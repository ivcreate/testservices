<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\Pay as Pay;
use Laravel\Lumen\Routing\Controller as BaseController;

class PayController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request){
        $pay = new Pay;
        $pay->generatePay();
    }
}
