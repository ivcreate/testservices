<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Jobs\PayJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    //php artisan queue:listen --queue=send_pay
    protected function schedule(Schedule $schedule)
    {
        $schedule->job((new PayJob())->onQueue('send_pay'))->cron('* * * * *');
        $schedule->job((new PayJob())->onQueue('send_pay')->delay(20))->cron('* * * * *');
        $schedule->job((new PayJob())->onQueue('send_pay')->delay(40))->cron('* * * * *');
        /*$schedule->call(function () {
            $job = (new PayJob())->onQueue('send_pay')->delay(20);
            $this->dispatch($job);
            //$job = (new PayJob())->onQueue('send_pay')->delay(20);
            //$this->dispatch($job);
            //$job = (new PayJob())->onQueue('send_pay')->delay(40);
            //$this->dispatch($job);
        })->cron('* * * * *');*/
    }
}
