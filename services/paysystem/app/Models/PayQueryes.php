<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayQueryes extends Model
{
    protected $table = 'pay_queryes';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param int $sum
     * @param float $commision
     * @param int $order_number
     * @param string $status
     */
    public function addPay(int $sum,
                           float $commision,
                           int $order_number,
                           string $status):void
    {
        $this->sum = $sum;
        $this->commision = $commision;
        $this->order_number = $order_number;
        $this->status = $status;

        $this->save();
    }

    /**
     * @param int $id
     * @param string $status
     */
    public function changeStatusPay(int $id,
                                 string $status):void
    {
        $pay = $this->find($id);

        $pay->status = $status;

        $pay->save();
    }

    /**
     * @return array
     */
    public function getWaitingPays():array
    {
        return app('db')->select("SELECT * FROM {$this->table} WHERE status = 'wait_ship'");
    }

    /**
     * @param string $ids
     * @param string $status
     */
    public function changeStatusPays(string $ids,
                                     string $status):void
    {

        app('db')->update("UPDATE {$this->table} SET `status`='{$status}' WHERE id IN ($ids)");
    }

}
