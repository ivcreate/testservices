<?php

namespace App\Jobs;

use App\Logic\Pay as Pay;

class PayJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pay = new Pay();
        return $pay->sendPay();

    }
}
