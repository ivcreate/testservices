<?php
namespace App\Logic;

use Mockery\Exception;

class Logic
{
    /**
     * @param json $data
     * @param string $url
     * @return bool
     */
    protected function postData(string $data,
                                string $uri):bool
    {
        $ch = curl_init(env("APP_URL").$uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        try {
            $result = curl_exec($ch);
        }catch (Exception $e){
            return false;
        }
        curl_close($ch);
        return true;
    }

    protected function generateToken():string
    {
        return hash("sha256",date("Y-m-d")."secret");
    }
}
