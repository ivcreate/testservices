<?php
namespace App\Logic;

use App\Logic\Logic as Logic;
use App\Models\PayQueryes as PayQuery;

class Pay extends Logic
{
    public function generatePay(): void
    {
       $count_pays = rand(1,10);
        for($i = 0; $i < $count_pays; $i++){
            $pay_query =  new PayQuery;
            $pay_query->addPay(rand(10,500),
            rand(0,1) + 1/rand(1,2),
            rand(1,20),
            'wait_ship');
        }
    }

    public function sendPay():bool
    {
        $pay_query = new PayQuery;
        $send_pays = ["token" => $this->generateToken(), "transactions" => []];
        $ids = [];
        foreach($pay_query->getWaitingPays() as $pay){
            $ids[] = $pay->id;
            $send_pays['transactions'][] = ["id" => $pay->id,
            "sum" => $pay->sum,
            "commision" => $pay->commision,
            "order_number" => $pay->order_number];
        }
        print_r(json_encode($send_pays));
        if($this->postData(json_encode($send_pays),"/services/acceptpay/public/")){
           $pay_query->changeStatusPays("'".implode("','",$ids)."'", "send");
           return true;
        }
        return false;
    }

}
