<?php
namespace App\Logic;

use Laravel\Lumen\Routing\Controller as BaseController;

class Logic extends BaseController
{
    public function checkToken(string $token):bool
    {
        if($this->generateToken() == $token)
            return true;
        return false;
    }

    protected function generateToken():string
    {
        return hash("sha256",date("Y-m-d")."secret");
    }
}
