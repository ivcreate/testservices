<?php

namespace App\Logic;

use App\Logic\Logic as Logic;
use App\Models\WalletModel;
use App\Models\UserTransaction as TransactionModel;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\WalletJob;

class Wallet extends Logic
{
    /**
     * @param string $json
     */
    public function process_json(string $json){
        $transactions = json_decode($json);
        if($this->checkToken($transactions->token)){
            foreach($transactions->transactions as $transaction){
                $sum = $this->getSum($transaction->sum,$transaction->commision);
                $trans_model = new TransactionModel();
                $trans_model->addTransaction($transaction->id,
                    $transaction->order_number,
                    $sum);
                $this->addJob($transaction->order_number,
                    $sum);
            }
        }
    }

    private function getSum(int $sum,
                            float $procent):float
    {
        return $sum - $sum*$procent/100;
    }

    /**
     * @param int $user_id
     * @param float $sum
     */
    public function addJob(int $user_id,
                           float $sum):void
    {
        $job = (new WalletJob(["user_id"=>$user_id, "sum"=>$sum]))->onQueue('add_pay_user')->delay(20);
        $this->dispatch($job);
    }

    public function addPayUser(int $user_id,
                               float $sum):void
    {
        $wallet_mod = new WalletModel();
        $wallet = $wallet_mod->getWalletByUser($user_id);
        if(empty($wallet))
            $wallet_mod->addWallet($user_id, $sum);
        else
            $wallet_mod->newSum($user_id, $wallet[0]->sum + $sum);
    }

}
