<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletModel extends Model
{
    protected $table = 'user_wallet';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param int $id
     * @param int $user_id
     * @param float $sum
     */
    public function addWallet(int $user_id,
                              float $sum):void
    {
        $this->user_id = $user_id;
        $this->sum = $sum;

        $this->save();
    }

    /**
     * @param int $user_id
     * @return array
     */
    public function getWalletByUser(int $user_id):array
    {
        return app('db')->select("SELECT * FROM {$this->table} WHERE user_id = '{$user_id}'");
    }

    public function newSum($user_id, $sum):void
    {
        app('db')->update("UPDATE {$this->table} SET `sum`='{$sum}' WHERE user_id = ($user_id)");
    }
}
