<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    protected $table = 'user_transaction';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param int $id
     * @param int $user_id
     * @param float $sum
     */
    public function addTransaction(int $id,
                                   int $user_id,
                                   float $sum):void
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->sum = $sum;

        $this->save();
    }
}
