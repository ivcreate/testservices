<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Logic\Wallet as Wallet;

class WalletController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request){
        $data = file_get_contents('php://input');
        $wallet = new Wallet;
        $wallet->process_json($data);
    }
}
