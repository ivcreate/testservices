<?php

namespace App\Jobs;

use App\Logic\Wallet as Wallet;

class WalletJob extends Job
{
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $wallet = new Wallet();
        $wallet->addPayUser($this->data['user_id'], $this->data['sum']);
    }
}
